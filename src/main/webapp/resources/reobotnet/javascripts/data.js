// funcao que calcula a idade em anos, meses e dias a partir da data_nascimento_informada
function calculaIdade(data_nascimento_informada) {
	var ano_nascimento, dia_nascimento, hoje, idade, mes_nascimento, anos_idade, dias_idade, meses_idade, str_ano, str_dia, str_idade, str_mes;
	data_nascimento_informada = data_nascimento_informada.split("/");
	dia_nascimento = data_nascimento_informada[0];
	mes_nascimento = data_nascimento_informada[1];
	mes_nascimento--;
	ano_nascimento = data_nascimento_informada[2];
	hoje = new Date();
	var data_nascimento = new Date(ano_nascimento, mes_nascimento,
			dia_nascimento);
	var differenceInMilisecond = hoje.valueOf() - data_nascimento.valueOf();
	anos_idade = Math.floor(differenceInMilisecond / 31536000000);
	dias_idade = Math.floor((differenceInMilisecond % 31536000000) / 86400000);
	meses_idade = Math.floor(dias_idade / 30);
	dias_idade = dias_idade % 30;
	if (isNaN(dias_idade) || isNaN(meses_idade) || isNaN(anos_idade)) {
		return null;
	}
	idade = {
		anos : anos_idade,
		meses : meses_idade,
		dias : dias_idade
	};
	if (idade.anos === 1) {
		str_ano = 'ano';
	} else {
		str_ano = 'anos';
	}
	if (idade.meses === 1) {
		str_mes = 'mês';
	} else {
		str_mes = "meses";
	}
	if (idade.dias === 1) {
		str_dia = 'dia';
	} else {
		str_dia = "dias";
	}
	str_idade = idade.anos + " " + str_ano + ", " + idade.meses + " " + str_mes
			+ " e " + idade.dias + " " + str_dia;
	return str_idade;
};