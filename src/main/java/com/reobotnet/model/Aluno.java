package com.reobotnet.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

@Entity
@Table(name = "aluno")
public class Aluno implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	private String nome;
	private String matricula;
	private StatusAluno statusAluno = StatusAluno.ESPERA;
	private Sexo sexo;
	private String nomeMae;
	private String cpfMae;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@NotBlank
	@Size(max = 80)
	@Column(nullable = false, length = 80)
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(nullable = false)
	public StatusAluno getStatusAluno() {
		return statusAluno;
	}

	public void setStatusAluno(StatusAluno statusAluno) {
		this.statusAluno = statusAluno;
	}
	
	public String getNomeMae() {
		return nomeMae;
	}

	public void setNomeMae(String nomeMae) {
		this.nomeMae = nomeMae;
	}

	public String getCpfMae() {
		return cpfMae;
	}

	public void setCpfMae(String cpfMae) {
		this.cpfMae = cpfMae;
	}
	
	
	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(nullable = false)
	public Sexo getSexo() {
		return sexo;
	}

	public void setSexo(Sexo sexo) {
		this.sexo = sexo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Aluno other = (Aluno) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	// Server para dizer se um aluno é novo
	@Transient
	public boolean isNovo() {
		return getId() == null;
	}

	// Serve para saber se existe um aluno
	@Transient
	public boolean isExistente() {
		return !isNovo();
	}

	// Verifica se o status do aluno está em espera
	@Transient
	public boolean isEspera() {
		return StatusAluno.ESPERA.equals(this.getStatusAluno());
	}

	// Verifica se o aluno está efetivo
	@Transient
	public boolean isEfetivo() {
		return StatusAluno.EFETIVO.equals(this.getStatusAluno());
	}

	// Verifica se um aluno não é efetivo
	@Transient
	public boolean isNaoEfetivo() {
		return !this.isEmissivel();
	}
	
	@Transient
	public boolean isEmissivel() {
		return this.isExistente() && this.isEspera();
	}

	// Verifica se um aluno pode ser cancelado
	@Transient
	public boolean isCancelavel() {
		return this.isExistente() && !this.isCancelado();
	}

	// Verifica se um aluno está cancelado
	@Transient
	private boolean isCancelado() {
		return StatusPedido.CANCELADO.equals(this.getStatusAluno());
	}

	// Diz que um aluno não pode ser cancelado
	@Transient
	public boolean isNaoCancelavel() {
		return !this.isCancelavel();
	}

}
