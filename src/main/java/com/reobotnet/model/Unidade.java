package com.reobotnet.model;

public enum Unidade {
	
	UN  ("un"),
	CX  ("cx"),
	PCT ("pct"),
	LT  ("lt");

	private String descricao;
	
	Unidade(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}


}
