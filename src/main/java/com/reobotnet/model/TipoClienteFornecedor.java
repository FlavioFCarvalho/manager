package com.reobotnet.model;

public enum TipoClienteFornecedor {
	
	CLIENTE("Cliente"),
	FORNECEDOR("Fornecedor");
	
	private String descricao;
	
	TipoClienteFornecedor(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}

}




