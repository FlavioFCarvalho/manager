package com.reobotnet.model;

public enum StatusPagamento {
	
	APAGAR("A pagar"),
	PAGO("Pago"),
	CANCELADO("Cancelado");
	
	private String descricao;


	StatusPagamento(String descricao) {
		this.descricao = descricao;
	}
	
	public String getDescricao() {
		return descricao;
	}

}
