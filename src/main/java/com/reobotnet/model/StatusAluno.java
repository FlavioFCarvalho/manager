package com.reobotnet.model;

public enum StatusAluno {
	

	ESPERA("Espera"), 
	EFETIVO("Efetivo"), 
	INATIVO("Inativo");
	
	private String descricao;
	
	StatusAluno(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}

}
