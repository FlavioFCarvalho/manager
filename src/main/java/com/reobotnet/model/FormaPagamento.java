package com.reobotnet.model;

public enum FormaPagamento {
	
	DINHEIRO("Dinheiro"),
	CHEQUE("Cheque"),
	CARTAO("Cartão");
	
	private String descricao;


	FormaPagamento(String descricao) {
		this.descricao = descricao;
	}
	
	public String getDescricao() {
		return descricao;
	}

}
