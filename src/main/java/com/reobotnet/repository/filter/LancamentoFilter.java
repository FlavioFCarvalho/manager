package com.reobotnet.repository.filter;

import java.io.Serializable;
import java.util.Date;

import com.reobotnet.model.StatusPagamento;



public class LancamentoFilter implements Serializable {

	private static final long serialVersionUID = 1L;

	private Date dataCriacaoDe;
	private Date dataCriacaoAte;
	private StatusPagamento[] statuses;
	

	public Date getDataCriacaoDe() {
		return dataCriacaoDe;
	}

	public void setDataCriacaoDe(Date dataCriacaoDe) {
		this.dataCriacaoDe = dataCriacaoDe;
	}

	public Date getDataCriacaoAte() {
		return dataCriacaoAte;
	}

	public void setDataCriacaoAte(Date dataCriacaoAte) {
		this.dataCriacaoAte = dataCriacaoAte;
	}

	public StatusPagamento[] getStatuses() {
		return statuses;
	}

	public void setStatuses(StatusPagamento[] statuses) {
		this.statuses = statuses;
	}
	
	
}
