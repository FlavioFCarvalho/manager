package com.reobotnet.repository;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.reobotnet.model.Aluno;
import com.reobotnet.repository.filter.AlunoFilter;

public class Alunos implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Inject
	private EntityManager manager;
	
	public Aluno guardar(Aluno aluno) {
		EntityTransaction trx = manager.getTransaction();
		trx.begin();
		
		aluno = manager.merge(aluno);
		
		trx.commit();
		
		return aluno;
	}


	@SuppressWarnings("unchecked")
	public List<Aluno> filtrados(AlunoFilter filtro) {
		Session session = manager.unwrap(Session.class);
		Criteria criteria = session.createCriteria(Aluno.class);
		
				
		if (StringUtils.isNotBlank(filtro.getNome())) {
			criteria.add(Restrictions.ilike("nome", filtro.getNome()));
		}
		
		return criteria.addOrder(Order.asc("nome")).list();
	}
	
	
	public Aluno porId(Long id) {
		return manager.find(Aluno.class, id);
	}
}