package com.reobotnet.repository;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import com.reobotnet.model.Marca;

public class Marcas implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Inject
	private EntityManager manager;
	
	public Marca guardar(Marca marca) {
		EntityTransaction trx = manager.getTransaction();
		trx.begin();
		
		marca = manager.merge(marca);
		
		trx.commit();
		
		return marca;
	}

	@SuppressWarnings("unchecked")
	public List<Marca> buscarTodos() {
		return manager.createQuery("from Marca").getResultList();
	}
	
	public Marca porId(Long id) {
		return manager.find(Marca.class, id);
	}
}
