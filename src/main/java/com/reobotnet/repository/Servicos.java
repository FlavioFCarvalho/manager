package com.reobotnet.repository;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import com.reobotnet.model.Servico;


public class Servicos implements Serializable{

	
	private static final long serialVersionUID = 1L;
	
	@Inject
	private EntityManager manager;
	
	public Servico guardar(Servico servico) {
		EntityTransaction trx = manager.getTransaction();
		trx.begin();
		
		servico = manager.merge(servico);
		
		trx.commit();
		
		return servico;
	}

	@SuppressWarnings("unchecked")
	public List<Servico> buscarTodos() {
		return manager.createQuery("from Servico").getResultList();
	}
	
	public Servico porId(Long id) {
		return manager.find(Servico.class, id);
	}

}
