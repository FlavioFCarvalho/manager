package com.reobotnet.repository;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.TypedQuery;


import org.hibernate.Criteria;
import org.hibernate.Session;

import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;


import com.reobotnet.model.Lancamento;
import com.reobotnet.repository.filter.LancamentoFilter;



public class Lancamentos implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	private EntityManager manager;


	public Lancamento porId(Long id) {
		return manager.find(Lancamento.class, id);
	}
	
	public List<String> descricoesQueContem(String descricao) {
		TypedQuery<String> query = manager.createQuery(
				"select distinct descricao from Lancamento "
				+ "where upper(descricao) like upper(:descricao)", 
				String.class);
		query.setParameter("descricao", "%" + descricao + "%");
		return query.getResultList();
	}
	/*
	public List<Lancamento> todos() {
		TypedQuery<Lancamento> query = manager.createQuery(
				"from Lancamento", Lancamento.class);
		return query.getResultList();
	}
	*/
	
	@SuppressWarnings("unchecked")
	public List<Lancamento> filtrados(LancamentoFilter filtro) {
		Session session = this.manager.unwrap(Session.class);
		
		Criteria criteria = session.createCriteria(Lancamento.class);
				
		
		if (filtro.getDataCriacaoDe() != null) {
			criteria.add(Restrictions.ge("dataVencimento", filtro.getDataCriacaoDe()));
		}
		
		if (filtro.getDataCriacaoAte() != null) {
			criteria.add(Restrictions.le("dataVencimento", filtro.getDataCriacaoAte()));
		}
				
		return criteria.addOrder(Order.asc("id")).list();
	}

	public void adicionar(Lancamento lancamento) {
		this.manager.persist(lancamento);
	}
	

	
	public Lancamento guardar(Lancamento lancamento) {
		EntityTransaction trx = manager.getTransaction();
		trx.begin();
		
		lancamento = manager.merge(lancamento);
		
		trx.commit();
		
		return lancamento;
	}
	
	

}