package com.reobotnet.repository;



import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.TypedQuery;

import com.reobotnet.model.Pessoa;


public class Pessoas implements Serializable  {

	private static final long serialVersionUID = 1L;
	
	@Inject
	private EntityManager manager;
	
	public Pessoa guardar(Pessoa pessoa) {
		EntityTransaction trx = manager.getTransaction();
		trx.begin();
		
		pessoa = manager.merge(pessoa);
		
		trx.commit();
		
		return pessoa;
	}

	public Pessoa porId(Long id) {
		return manager.find(Pessoa.class, id);
	}
	
	public List<Pessoa> todas() {
		TypedQuery<Pessoa> query = manager.createQuery(
				"from Pessoa", Pessoa.class);
		return query.getResultList();
	}

	
	public List<Pessoa> porNome(String nome) {
		return this.manager.createQuery("from Pessoa " +
				"where upper(nome) like :nome", Pessoa.class)
				.setParameter("nome", nome.toUpperCase() + "%")
				.getResultList();
	}
		
}
