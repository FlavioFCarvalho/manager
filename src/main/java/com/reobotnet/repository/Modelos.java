package com.reobotnet.repository;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import com.reobotnet.model.Modelo;

public class Modelos implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Inject
	private EntityManager manager;
	
	public Modelo guardar(Modelo modelo) {
		EntityTransaction trx = manager.getTransaction();
		trx.begin();
		
		modelo = manager.merge(modelo);
		
		trx.commit();
		
		return modelo;
	}
	
	@SuppressWarnings("unchecked")
	public List<Modelo> buscarTodos() {
		return manager.createQuery("from Modelo").getResultList();
	}
	
	public Modelo porId(Long id) {
		return manager.find(Modelo.class, id);
	}

	
}
