package com.reobotnet.repository;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;


import com.reobotnet.model.Pedido;

public class Pedidos implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Inject
	private EntityManager manager;
	
	public Pedido guardar(Pedido pedido) {
		EntityTransaction trx = manager.getTransaction();
		trx.begin();
		
		pedido = manager.merge(pedido);
		
		trx.commit();
		
		return pedido;
	}
	
	@SuppressWarnings("unchecked")
	public List<Pedido> buscarTodos() {
		
		return manager.createQuery("from Pedido").getResultList();
	}


	public Pedido porId(Long id) {
		return manager.find(Pedido.class, id);
	}

	
}