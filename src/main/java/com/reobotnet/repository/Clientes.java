package com.reobotnet.repository;

import java.io.Serializable;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import com.reobotnet.model.Cliente;


public class Clientes implements Serializable{

	
	private static final long serialVersionUID = 1L;
	
	@Inject
	private EntityManager manager;

	
	public Cliente guardar(Cliente cliente) {
		EntityTransaction trx = manager.getTransaction();
		trx.begin();
		
		cliente = manager.merge(cliente);
		
		trx.commit();
		
		return cliente;
	}
	

	

}
