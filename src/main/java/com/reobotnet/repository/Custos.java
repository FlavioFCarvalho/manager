package com.reobotnet.repository;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import com.reobotnet.model.Custo;


public class Custos implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Inject
	private EntityManager manager;
	
	public Custo guardar(Custo custo) {
		EntityTransaction trx = manager.getTransaction();
		trx.begin();
		
		custo = manager.merge(custo);
		
		trx.commit();
		
		return custo;
	}

	@SuppressWarnings("unchecked")
	public List<Custo> buscarTodos() {
		return manager.createQuery("from Custo").getResultList();
	}

	public Custo porId(Long id) {
		
		return manager.find(Custo.class, id);
			
	}
	
	
}
