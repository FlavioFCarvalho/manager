package com.reobotnet.repository;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import com.reobotnet.model.Banco;


public class Bancos implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private EntityManager manager;

	@SuppressWarnings("unchecked")
	public List<Banco> buscarTodos() {
		return manager.createQuery("from Banco").getResultList();
	}

	public Banco porId(Long id) {

		return manager.find(Banco.class, id);

	}

}
