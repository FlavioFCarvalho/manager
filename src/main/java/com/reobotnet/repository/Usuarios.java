package com.reobotnet.repository;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import com.reobotnet.model.Produto;
import com.reobotnet.model.Usuario;

public class Usuarios implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Inject
	private EntityManager manager;
	
	
	public Usuario guardar(Usuario usuario) {
		EntityTransaction trx = manager.getTransaction();
		trx.begin();
		
		usuario = manager.merge(usuario);
		
		trx.commit();
		
		return usuario;
	}


	@SuppressWarnings("unchecked")
	public List<Usuario> buscarTodos() {
		
		return manager.createQuery("from Usuario").getResultList();
	}

	public List<Usuario> vendedores() {
		
		// filtrar apenas vendedores (por um grupo específico)
		return this.manager.createQuery("from Usuario", Usuario.class)
				.getResultList();
			
	}

	public Usuario porId(Long id) {
		
		return manager.find(Usuario.class,id);
	}

}
