package com.reobotnet.repository;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;

import com.reobotnet.model.Produto;



public class Produtos implements Serializable{

	
	private static final long serialVersionUID = 1L;
	
	@Inject
	private EntityManager manager;
	
	
	public Produto guardar(Produto produto) {
		EntityTransaction trx = manager.getTransaction();
		trx.begin();
		
		produto = manager.merge(produto);
		
		trx.commit();
		
		return produto;
	}

		
	@SuppressWarnings("unchecked")
	public List<Produto> buscarTodos() {
		
		return manager.createQuery("from Produto").getResultList();
	}


	public Produto porId(Long id) {
		return manager.find(Produto.class, id);
	}
	
	public List<Produto> porNome(String nome) {
		return this.manager.createQuery("from Produto where upper(nome) like :nome", Produto.class)
				.setParameter("nome", nome.toUpperCase() + "%").getResultList();
	}
	
	public Produto porSku(String sku) {
		try {
			return manager.createQuery("from Produto where upper(sku) = :sku", Produto.class)
				.setParameter("sku", sku.toUpperCase())
				.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}
}
