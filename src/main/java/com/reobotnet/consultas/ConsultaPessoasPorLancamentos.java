package com.reobotnet.consultas;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import com.reobotnet.util.JPAUtil;

public class ConsultaPessoasPorLancamentos {

	public static void main(String[] args) {

		EntityManagerFactory emf = JPAUtil.createEntityManager()
				.getEntityManagerFactory();
		EntityManager em = emf.createEntityManager();

		List<String> nomeDasPessoas = em.createQuery(
				"select lc.pessoa.nome from Lancamento lc", String.class)
				.getResultList();

		for (String nomePessoa : nomeDasPessoas) {
			System.out.println("Pessoa: " + nomePessoa);
		}

		em.close();

	}

}
