package com.reobotnet.consultas;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import com.reobotnet.util.JPAUtil;



public class ConsultaPessoasPassandoParametros {
	
	public static void main(String[] args) {
		EntityManagerFactory emf = JPAUtil.createEntityManager().getEntityManagerFactory();
		EntityManager em = emf.createEntityManager();
		
		String modelo = "Carvalho";

		String jpql = "select lc.pessoa from Lancamento lc "
				        + "where lc.nome = :modelo";
		List<String> modelos = em.createQuery(jpql, String.class)
				.setParameter("modelo", modelo)
				.getResultList();
		
		for (String m : modelos) {
			System.out.println(m);
		}
		
		em.close();
	}

}
