package com.reobotnet.util;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JPAUtil {

	private static EntityManagerFactory emf = Persistence.createEntityManagerFactory("ManagerPU");
	
	public static EntityManager createEntityManager() {
		return emf.createEntityManager();
	}
	
}
