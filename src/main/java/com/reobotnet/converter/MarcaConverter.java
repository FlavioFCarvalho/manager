package com.reobotnet.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import com.reobotnet.model.Marca;
import com.reobotnet.repository.Marcas;
import com.reobotnet.util.cdi.CDIServiceLocator;




@FacesConverter(forClass = Marca.class)
public class MarcaConverter implements Converter {

	//@Inject
	private Marcas marcas;
	
	
	public MarcaConverter() {
		marcas = CDIServiceLocator.getBean(Marcas.class);
			
	}
	
		
	
	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		Marca retorno = null;
		
		if (value != null) {
			Long id = new Long(value);
			retorno = marcas.porId(id);
		}
		
		return retorno;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value != null) {
			Marca marca = (Marca) value;
			return marca.getId() == null ? null : marca.getId().toString();
		}
		
		return "";
	}

}