package com.reobotnet.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import com.reobotnet.model.Custo;
import com.reobotnet.repository.Custos;
import com.reobotnet.util.cdi.CDIServiceLocator;




@FacesConverter(forClass = Custo.class)
public class CustoConverter implements Converter {

	//@Inject
	private Custos custos;
	
	
	public CustoConverter() {
		custos = CDIServiceLocator.getBean(Custos.class);
			
	}
	
		
	
	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		Custo retorno = null;
		
		if (value != null) {
			Long id = new Long(value);
			retorno = custos.porId(id);
		}
		
		return retorno;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value != null) {
			Custo custo = (Custo) value;
			return  custo.getId() == null ? null : custo.getId().toString();
		}
		
		return "";
	}

}