package com.reobotnet.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import com.reobotnet.model.Modelo;
import com.reobotnet.repository.Modelos;
import com.reobotnet.util.cdi.CDIServiceLocator;




@FacesConverter(forClass = Modelo.class)
public class ModeloConverter implements Converter {

	//@Inject
	private Modelos modelos;
	
	
	public ModeloConverter() {
		modelos = CDIServiceLocator.getBean(Modelos.class);
			
	}
			
	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		Modelo retorno = null;
		
		if (value != null) {
			Long id = new Long(value);
			retorno = modelos.porId(id);
			
		}
		
		return retorno;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value != null) {
			Modelo modelo = (Modelo) value;
			return modelo.getId() == null ? null : modelo.getId().toString();
		}
		
		return "";
	}

}