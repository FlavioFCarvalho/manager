package com.reobotnet.service;

import java.io.Serializable;

import javax.inject.Inject;

import com.reobotnet.model.Aluno;
import com.reobotnet.model.StatusAluno;
import com.reobotnet.repository.Alunos;

public class InativarAlunoService implements Serializable{

	
	private static final long serialVersionUID = 1L;
	
	@Inject
	private Alunos alunos;
	
	
	
	public Aluno inativar(Aluno aluno)  {
		aluno = this.alunos.porId(aluno.getId());
		
		if (aluno.isNaoCancelavel()) {
			throw new NegocioException("Pedido não pode ser cancelado no status "
					+ aluno.getStatusAluno().getDescricao() + ".");
		}
		
		aluno.setStatusAluno(StatusAluno.INATIVO);
		
		aluno = this.alunos.guardar(aluno);
		
			
		return aluno;
	}
	

}
