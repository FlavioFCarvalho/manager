package com.reobotnet.service;

import java.io.Serializable;

import javax.inject.Inject;

import com.reobotnet.model.Cliente;
import com.reobotnet.repository.Clientes;


public class CadastroClienteService implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private Clientes clientes;

	public Cliente salvar(Cliente cliente) throws NegocioException {

		if (cliente.getNome() == null || cliente.getNome().trim().equals("")) {
			throw new NegocioException("O Nome do cliente é obrigatório");
		}

		return clientes.guardar(cliente);
	}

}
