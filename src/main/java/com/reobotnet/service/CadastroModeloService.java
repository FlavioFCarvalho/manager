package com.reobotnet.service;

import java.io.Serializable;

import javax.inject.Inject;

import com.reobotnet.model.Modelo;
import com.reobotnet.repository.Modelos;


public class CadastroModeloService implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private Modelos modelos;

	public Modelo salvar(Modelo modelo) throws NegocioException {

		if (modelo.getNome() == null || modelo.getNome().trim().equals("")) {
			throw new NegocioException("O Nome do cliente é obrigatório");
		}

		return modelos.guardar(modelo);
	}

}
