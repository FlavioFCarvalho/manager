package com.reobotnet.service;

import java.io.Serializable;

import javax.inject.Inject;

import com.reobotnet.model.Marca;
import com.reobotnet.repository.Marcas;



public class CadastroMarcaService implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private Marcas marcas;

	public Marca salvar(Marca marca) throws NegocioException {

		if (marca.getNome() == null || marca.getNome().trim().equals("")) {
			throw new NegocioException("O Nome do cliente é obrigatório");
		}

		return marcas.guardar(marca);
	}

}
