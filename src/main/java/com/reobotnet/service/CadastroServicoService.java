package com.reobotnet.service;

import java.io.Serializable;

import javax.inject.Inject;

import com.reobotnet.model.Aluno;
import com.reobotnet.model.Servico;
import com.reobotnet.repository.Alunos;
import com.reobotnet.repository.Servicos;

public class CadastroServicoService implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Inject
	private Servicos servicos;

	public Servico salvar(Servico servico) throws NegocioException {

		if (servico.getNome() == null || servico.getNome().trim().equals("")) {
			throw new NegocioException("O Nome do servico é obrigatório");
		}

		return servicos.guardar(servico);
	}

}
