package com.reobotnet.service;

import java.io.Serializable;
import java.util.Date;

import javax.inject.Inject;

import com.reobotnet.model.Lancamento;
import com.reobotnet.repository.LancamentosOld;

public class CadastroLancamentosService implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	private LancamentosOld lancamentos;
	
	
	public void salvar(Lancamento lancamento) throws NegocioException {
		if (lancamento.getDataPagamento() != null &&
				lancamento.getDataPagamento().after(new Date())) {
			throw new NegocioException(
					"Data de pagamento não pode ser uma data futura.");
		}
		
		this.lancamentos.guardar(lancamento);
	}
	
		
}
