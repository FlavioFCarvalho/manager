package com.reobotnet.service;

import java.io.Serializable;

import javax.inject.Inject;

import com.reobotnet.model.Pedido;
import com.reobotnet.model.StatusPedido;
import com.reobotnet.repository.Pedidos;
import com.reobotnet.util.Transactional;



public class EmissaoPedidoService implements Serializable{

	
	private static final long serialVersionUID = 1L;
	
	@Inject
	private CadastroPedidoService cadastroPedidoService;
	
	@Inject
	private Pedidos pedidos;
    
	@Inject
	private EstoqueService estoqueService;
	
	@Transactional
	public Pedido emitir(Pedido pedido) throws NegocioException{
		
		pedido = this.cadastroPedidoService.salvar(pedido);
		
		if(pedido.isNaoEmissivel()){
			
			throw new NegocioException(" Pedido não pode ser emitido com o status"
					 + pedido.getStatus().getDescricao() + ".");
		}
		
		this.estoqueService.baixarItensEstoque(pedido);
		
		pedido.setStatus(StatusPedido.EMITIDO);
		
		pedido = this.pedidos.guardar(pedido);
		
		return pedido;
	
	
	}
	
	

}
