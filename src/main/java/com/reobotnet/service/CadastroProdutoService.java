package com.reobotnet.service;

import java.io.Serializable;

import javax.inject.Inject;

import com.reobotnet.model.Produto;
import com.reobotnet.repository.Produtos;




public class CadastroProdutoService implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Inject
	private Produtos produtos;
	
	public Produto salvar(Produto produto) throws NegocioException {
		
		
		if(produto.getNome() == null ||produto.getNome().trim().equals("")){
			throw new NegocioException("O Nome do cliente é obrigatório");
		}
		
		return produtos.guardar(produto);
	}
}
