package com.reobotnet.service;

import java.io.Serializable;

import javax.inject.Inject;

import com.reobotnet.model.Custo;
import com.reobotnet.repository.Custos;


public class CadastroCustoService implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private Custos custos;

	public Custo salvar(Custo custo) throws NegocioException {

		if (custo.getNome() == null || custo.getNome().trim().equals("")) {
			throw new NegocioException("O custo é obrigatório");
		}

		return custos.guardar(custo);
	}

}
