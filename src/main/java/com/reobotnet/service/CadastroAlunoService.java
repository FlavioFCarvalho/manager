package com.reobotnet.service;

import java.io.Serializable;

import javax.inject.Inject;

import com.reobotnet.model.Aluno;
import com.reobotnet.repository.Alunos;



public class CadastroAlunoService implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private Alunos alunos;

	public Aluno salvar(Aluno aluno) throws NegocioException {

		if (aluno.getNome() == null || aluno.getNome().trim().equals("")) {
			throw new NegocioException("O Nome do aluno é obrigatório");
		}

		return alunos.guardar(aluno);
	}

}
