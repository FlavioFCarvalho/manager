package com.reobotnet.service;

import java.io.Serializable;

import javax.inject.Inject;

import com.reobotnet.model.Aluno;
import com.reobotnet.model.StatusAluno;
import com.reobotnet.repository.Alunos;
import com.reobotnet.util.Transactional;

public class EfetivaAlunoService implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Inject
	private CadastroAlunoService cadastroAlunoService;
	
	@Inject
	private Alunos alunos;
    
	@Transactional
	public Aluno efetivar(Aluno aluno) {
				
		aluno = this.cadastroAlunoService.salvar(aluno);
		
		if(aluno.isNaoEfetivo()){
			
			throw new NegocioException(" O aluno não pode ser efetivado com o status"
				 + aluno.getStatusAluno().getDescricao() + ".");
		}
		
		aluno.setStatusAluno(StatusAluno.EFETIVO);		
		aluno = this.alunos.guardar(aluno);
		
		return aluno;
	
	
	}

}
