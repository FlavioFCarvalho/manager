package com.reobotnet.service;

import java.io.Serializable;

import javax.inject.Inject;

import com.reobotnet.model.Orcamento;
import com.reobotnet.repository.Orcamentos;
import com.reobotnet.util.Transacional;



public class CadastroOrcamentoService implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	private Orcamentos orcamentos;
	
	@Transacional
	public void salvar(Orcamento orcamento) {
		orcamentos.guardar(orcamento);
	}
	
}