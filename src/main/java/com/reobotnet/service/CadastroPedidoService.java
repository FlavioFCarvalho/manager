package com.reobotnet.service;

import java.io.Serializable;
import java.util.Date;

import javax.inject.Inject;

import com.reobotnet.model.Pedido;
import com.reobotnet.model.StatusPedido;
import com.reobotnet.repository.Pedidos;


public class CadastroPedidoService implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private Pedidos pedidos;
	


	
	public Pedido salvar(Pedido pedido) throws NegocioException  {
		
		if (pedido.isNovo()) {
			pedido.setDataCriacao(new Date());
			pedido.setStatus(StatusPedido.ORCAMENTO);
		}
		
		pedido.recalcularValorTotal();
		
		if (pedido.getItens().isEmpty()) {
			throw new NegocioException("O pedido deve possuir pelo menos um item.");
		}
		
		if (pedido.isValorTotalNegativo()) {
			throw new NegocioException("Valor total do pedido não pode ser negativo.");
		}
		
		pedido = this.pedidos.guardar(pedido);
		return pedido;
	}
}
