package com.reobotnet.service;

import java.io.Serializable;

import javax.inject.Inject;

import com.reobotnet.model.Pessoa;
import com.reobotnet.repository.Pessoas;


public class CadastroPessoaService implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private Pessoas pessoas;

	public Pessoa salvar(Pessoa pessoa) throws NegocioException {

		if (pessoa.getNome() == null || pessoa.getNome().trim().equals("")) {
			throw new NegocioException("O nome é obrigatório");
		}

		return pessoas.guardar(pessoa);
	}
	
}
