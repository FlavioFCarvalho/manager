package com.reobotnet.controller;

import java.io.Serializable;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.reobotnet.model.Cliente;
import com.reobotnet.service.CadastroClienteService;
import com.reobotnet.service.NegocioException;
import com.reobotnet.util.FacesUtil;

@Named
@ViewScoped
public class CadastroClienteBean implements Serializable{

	
	private static final long serialVersionUID = 1L;
	
	@Inject
	private CadastroClienteService cadastroClienteService;
	
	private Cliente cliente ;
	
	public CadastroClienteBean() {
		limpar();
	}
	
	private void limpar() {
		cliente = new Cliente();
	}
	
		
	public void salvar() throws NegocioException{
		this.cliente = cadastroClienteService.salvar(cliente);
		limpar();
		FacesUtil.addInfoMessage("Cliente salvo com sucesso!");
	}

	public Cliente getCliente() {
		return cliente;
	}
	
	
	

}
