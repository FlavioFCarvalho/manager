package com.reobotnet.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.reobotnet.model.Custo;
import com.reobotnet.repository.Custos;


@Named
@ViewScoped
public class PesquisaCustosBean implements Serializable{

	
	private static final long serialVersionUID = 1L;
	
	@Inject
	Custos custos;
	
	private List<Custo> custosList = new ArrayList<>();
	
	
	public Custos getCustos() {
		return custos;
	}


	public void setCustos(Custos custos) {
		this.custos = custos;
	}


	public List<Custo> getCustosList() {
		return custosList;
	}


	public void setCustosList(List<Custo> custosList) {
		this.custosList = custosList;
	}


	@PostConstruct
	public void inicializar() {
		 custosList = custos.buscarTodos();
	}
	
}
