package com.reobotnet.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.reobotnet.model.Modelo;
import com.reobotnet.model.Pedido;
import com.reobotnet.repository.Modelos;
import com.reobotnet.repository.Pedidos;



@Named
@ViewScoped
public class PesquisaPedidosBean implements Serializable{

	
	private static final long serialVersionUID = 1L;
	
	@Inject
	Pedidos pedidos;
	
	private List<Pedido> pedidosList = new ArrayList<>();
	
	
	public Pedidos getPedidos() {
		return pedidos;
	}


	public void setPedidos(Pedidos pedidos) {
		this.pedidos = pedidos;
	}


	public List<Pedido> getPedidosList() {
		return pedidosList;
	}


	public void setPedidosList(List<Pedido> pedidosList) {
		this.pedidosList = pedidosList;
	}

	@PostConstruct
	public void inicializar() {
		 pedidosList = pedidos.buscarTodos();
	}
	
}
