package com.reobotnet.controller;

import java.io.Serializable;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.inject.Named;

import com.reobotnet.model.Aluno;
import com.reobotnet.service.InativarAlunoService;
import com.reobotnet.util.FacesUtil;

@Named
@RequestScoped
public class InativarAlunoBean implements Serializable {


	private static final long serialVersionUID = 1L;

	@Inject
	private InativarAlunoService inativarAlunoService;
	
	@Inject
	private Event<AlunoAlteradoEvent> alunoAlteradoEvent;
	
	@Inject
	@AlunoEdicao
	private Aluno aluno;
	
	public void inativarAluno()  {
		this.aluno = this.inativarAlunoService.inativar(this.aluno);
		this.alunoAlteradoEvent.fire(new AlunoAlteradoEvent(this.aluno));
		
		FacesUtil.addInfoMessage("Aluno inativado com sucesso!");
	}
	
}