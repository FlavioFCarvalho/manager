package com.reobotnet.controller;

import com.reobotnet.model.Aluno;


public class AlunoAlteradoEvent {
	
	private Aluno aluno;

	public AlunoAlteradoEvent(Aluno aluno) {
		
		this.aluno = aluno;
	}

	public Aluno getAluno() {
		return aluno;
	}

	
}
