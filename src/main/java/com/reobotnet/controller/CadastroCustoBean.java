package com.reobotnet.controller;

import java.io.Serializable;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.reobotnet.model.Custo;
import com.reobotnet.service.CadastroCustoService;
import com.reobotnet.service.NegocioException;
import com.reobotnet.util.FacesUtil;



@Named
@ViewScoped
public class CadastroCustoBean implements Serializable {


	private static final long serialVersionUID = 1L;
	
	
	@Inject
	private CadastroCustoService cadastroCustoService;
	
	private Custo custo;
	
	
	
	public CadastroCustoBean() {
		limpar();
	}
	
	private void limpar() {
		custo = new Custo();
	}
	
		
	public void salvar() throws NegocioException{
		this.custo = cadastroCustoService.salvar(this.custo);
		limpar();
		FacesUtil.addInfoMessage("Marca salva com sucesso!");
	}

	public Custo getCusto() {
		return custo;
	}

	public void setCusto(Custo custo) {
		this.custo = custo;
	}

	

}
