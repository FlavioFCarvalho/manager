package com.reobotnet.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;


import com.reobotnet.model.Servico;
import com.reobotnet.repository.Servicos;


@Named
@ViewScoped
public class PesquisaServicosBean implements Serializable{

	
	private static final long serialVersionUID = 1L;
	
	@Inject
	Servicos servicos;
	
	private List<Servico> servicosList = new ArrayList<>();
	
	
	
	public List<Servico> getServicosList() {
		return servicosList;
	}
	

		
	public Servicos getServicos() {
		return servicos;
	}



	public void setServicos(Servicos servicos) {
		this.servicos = servicos;
	}



	public void setServicosList(List<Servico> servicosList) {
		this.servicosList = servicosList;
	}



	@PostConstruct
	public void inicializar() {
		 servicosList = servicos.buscarTodos();
	}
	
}
