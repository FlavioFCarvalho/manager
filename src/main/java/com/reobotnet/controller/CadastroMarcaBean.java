package com.reobotnet.controller;

import java.io.Serializable;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.reobotnet.model.Marca;
import com.reobotnet.service.CadastroMarcaService;
import com.reobotnet.service.NegocioException;
import com.reobotnet.util.FacesUtil;





@Named
@ViewScoped
public class CadastroMarcaBean implements Serializable {


	private static final long serialVersionUID = 1L;
	
	
	@Inject
	private CadastroMarcaService cadastroMarcaService;
	
	private Marca marca;
	
	
	
	public CadastroMarcaBean() {
		limpar();
	}
	
	private void limpar() {
		marca = new Marca();
	}
	
		
	public void salvar() throws NegocioException{
		this.marca = cadastroMarcaService.salvar(this.marca);
		limpar();
		FacesUtil.addInfoMessage("Marca salva com sucesso!");
	}

	public Marca getMarca() {
		return marca;
	}

	public void setMarca(Marca marca) {
		this.marca = marca;
	}

}
