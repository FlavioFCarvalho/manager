package com.reobotnet.controller;

import java.io.Serializable;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.reobotnet.model.Produto;
import com.reobotnet.service.CadastroProdutoService;
import com.reobotnet.service.NegocioException;
import com.reobotnet.util.FacesUtil;





@Named
@ViewScoped
public class CadastroProdutoBean implements Serializable {


	private static final long serialVersionUID = 1L;
	
	
	@Inject
	private CadastroProdutoService cadastroProdutoService;
	
	private Produto produto;
	
	
	
	public CadastroProdutoBean() {
		limpar();
	}
	
	private void limpar() {
		produto = new Produto();
	}
	
		
	public void salvar() throws NegocioException{
		this.produto = cadastroProdutoService.salvar(this.produto);
		limpar();
		FacesUtil.addInfoMessage("Produto salvo com sucesso!");
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}
		
	
}
