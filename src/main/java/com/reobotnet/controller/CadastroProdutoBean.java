package com.reobotnet.controller;

import java.io.Serializable;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.event.FlowEvent;


import com.reobotnet.model.Marca;
import com.reobotnet.model.Modelo;
import com.reobotnet.model.Pessoa;
import com.reobotnet.model.Produto;
import com.reobotnet.model.Status;
import com.reobotnet.model.Unidade;
import com.reobotnet.repository.Marcas;
import com.reobotnet.repository.Modelos;
import com.reobotnet.repository.Pessoas;
import com.reobotnet.service.CadastroProdutoService;
import com.reobotnet.service.NegocioException;
import com.reobotnet.util.FacesUtil;





@Named
@ViewScoped
public class CadastroProdutoBean implements Serializable {


	private static final long serialVersionUID = 1L;
	
	
	@Inject
	private CadastroProdutoService cadastroProdutoService;
	
	@Inject
	private Marcas marcas;
	
	@Inject
	private Modelos modelos;
	
	@Inject
	private Pessoas pessoas;
	
	private Produto produto;
	
	private boolean skip;
	
	private List<Marca> todasMarcas;
	
	private List<Modelo> todosModelos;
	
	private List<Pessoa> todasPessoas;
	
		
	public void prepararCadastro() {
		
		this.todasMarcas =  this.marcas.buscarTodos();
		this.todosModelos = this.modelos.buscarTodos();
		this.todasPessoas = this.pessoas.todas();
		
		if (this.produto == null) {
			this.produto = new Produto();
		}
	}
	
	
	public CadastroProdutoBean() {
		limpar();
	}
	
	private void limpar() {
		produto = new Produto();
	}
	
		
	public void salvar() throws NegocioException{
		this.produto = cadastroProdutoService.salvar(this.produto);
		limpar();
		FacesUtil.addInfoMessage("Produto salvo com sucesso!");
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}
	 
		
	public List<Marca> getTodasMarcas() {
		return todasMarcas;
	}
	
	
	public List<Modelo> getTodosModelos() {
		return todosModelos;
	}
		
    
	public List<Pessoa> getTodasPessoas() {
		return todasPessoas;
	}


	public Status[] getStatus() {
		return Status.values();
	}

	public Unidade[] getUnidade() {
		return Unidade.values();
	}
	
	public boolean isSkip() {
	        return skip;
	    }
	 
	    public void setSkip(boolean skip) {
	        this.skip = skip;
	    }
	     
	    public String onFlowProcess(FlowEvent event) {
	        if(skip) {
	            skip = false;   //reset in case user goes back
	            return "confirm";
	        }
	        else {
	            return event.getNewStep();
	        }
	    }
	
}
