package com.reobotnet.controller;

import java.io.Serializable;




import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;


import org.primefaces.event.FlowEvent;

import com.reobotnet.model.Usuario;
import com.reobotnet.service.CadastroUsuarioService;
import com.reobotnet.service.NegocioException;
import com.reobotnet.util.FacesUtil;

 
@Named
@ViewScoped
public class CadastroUsuarioBean implements Serializable {
 
	private static final long serialVersionUID = 1L;
	
	@Inject
	private CadastroUsuarioService cadastroUsuarioService;

	
	private Usuario usuario ;
     
    private boolean skip;
    
    public CadastroUsuarioBean() {
		limpar();
	}
	
	private void limpar() {
		usuario = new Usuario();
	}
     
        
    public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	/*
	public void save() {    
		
        FacesMessage msg = new FacesMessage("Successful", "Welcome :" + usuario.getNome());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }*/
     
	
	public void salvar() throws NegocioException{
		this.usuario = cadastroUsuarioService.salvar(this.usuario);
		limpar();
		FacesUtil.addInfoMessage("Usuário salvo com sucesso!");
	}
	
	
    public boolean isSkip() {
        return skip;
    }
 
    public void setSkip(boolean skip) {
        this.skip = skip;
    }
     
    public String onFlowProcess(FlowEvent event) {
        if(skip) {
            skip = false;   //reset in case user goes back
            return "confirm";
        }
        else {
            return event.getNewStep();
        }
    }
    
}   
