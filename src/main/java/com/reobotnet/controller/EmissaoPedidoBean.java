package com.reobotnet.controller;

import java.io.Serializable;


import javax.inject.Inject;
import javax.inject.Named;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.event.Event;

import com.reobotnet.model.Pedido;
import com.reobotnet.service.EmissaoPedidoService;
import com.reobotnet.service.NegocioException;
import com.reobotnet.util.FacesUtil;


@Named
@RequestScoped
public class EmissaoPedidoBean  implements Serializable{

	
	private static final long serialVersionUID = 1L;
	
	@Inject
	private EmissaoPedidoService emissaoPedidoService;
	
	@Inject
	@PedidoEdicao
	private Pedido pedido;
	
	@Inject
	private Event<PedidoAlteradoEvent> pedidoAlteradoEvent;
	
	//@Transacional
	public void emitirPedido()  {
		this.pedido.removerItemVazio();
		
		try {
			this.pedido = this.emissaoPedidoService.emitir(this.pedido);
			this.pedidoAlteradoEvent.fire(new PedidoAlteradoEvent(this.pedido));
			
			FacesUtil.addInfoMessage("Pedido emitido com sucesso!");
		} finally {
			this.pedido.adicionarItemVazio();
		}
	}

}

