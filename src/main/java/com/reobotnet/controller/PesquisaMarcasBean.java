package com.reobotnet.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.reobotnet.model.Marca;
import com.reobotnet.repository.Marcas;



@Named
@ViewScoped
public class PesquisaMarcasBean implements Serializable{

	
	private static final long serialVersionUID = 1L;
	
	@Inject
	Marcas marcas;
	
	private List<Marca> marcasList = new ArrayList<>();
	
	
	public List<Marca> getMarcasList() {
		return marcasList;
	}



	public void setMarcasList(List<Marca> marcasList) {
		this.marcasList = marcasList;
	}


    
	public Marcas getMarcas() {
		return marcas;
	}


	public void setMarcas(Marcas marcas) {
		this.marcas = marcas;
	}



	@PostConstruct
	public void inicializar() {
		 marcasList = marcas.buscarTodos();
	}
	
}
