package com.reobotnet.controller;

import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;


import com.reobotnet.model.Pessoa;
import com.reobotnet.model.Status;
import com.reobotnet.model.TipoClienteFornecedor;
import com.reobotnet.model.TipoPessoa;
import com.reobotnet.service.CadastroPessoaService;
import com.reobotnet.service.NegocioException;



@Named
@ViewScoped
public class CadastroPessoaBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	private CadastroPessoaService cadastroPessoaService;
	
			
	private Pessoa pessoa;
	

	public void prepararCadastro() {
		if (this.pessoa == null) {
			this.pessoa = new Pessoa();
		}
	}
	
			
	public void salvar() {
		FacesContext context = FacesContext.getCurrentInstance();
		
		try {
			this.cadastroPessoaService.salvar(this.pessoa);
			
			this.pessoa = new Pessoa();
			context.addMessage(null, new FacesMessage("Pessoa salva com sucesso!"));
		} catch (NegocioException e) {
			
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}
	}


	public Pessoa getPessoa() {
		return pessoa;
	}


	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}
	
	public TipoClienteFornecedor[] getTipoClienteFornecedores() {
		return TipoClienteFornecedor.values();
	}
	
	public TipoPessoa[] getTipoPessoa() {
		return TipoPessoa.values();
	}
	
	public Status[] getStatus() {
		return Status.values();
	}
	
	
	
}
