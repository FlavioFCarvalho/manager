package com.reobotnet.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.reobotnet.model.Pessoa;
import com.reobotnet.repository.Pessoas;



@Named
@ViewScoped
public class PesquisaPessoasBean implements Serializable{

	
	private static final long serialVersionUID = 1L;
	
	@Inject
	Pessoas pessoas;
	
	private List<Pessoa> pessoasList = new ArrayList<>();
	
	
	
	public List<Pessoa> getProdutosList() {
		return pessoasList;
	}
	
		
	public Pessoas getPessoas() {
		return pessoas;
	}


	public void setPessoas(Pessoas pessoas) {
		this.pessoas = pessoas;
	}


	public void setPessoasList(List<Pessoa> pessoasList) {
		this.pessoasList = pessoasList;
	}



	@PostConstruct
	public void inicializar() {
		 pessoasList = pessoas.todas();
	}
	
}
