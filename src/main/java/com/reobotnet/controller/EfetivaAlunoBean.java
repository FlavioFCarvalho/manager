package com.reobotnet.controller;

import java.io.Serializable;

import javax.inject.Inject;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.event.Event;

import com.reobotnet.model.Aluno;
import com.reobotnet.service.EfetivaAlunoService;
import com.reobotnet.util.FacesUtil;


@Named
@RequestScoped
public class EfetivaAlunoBean  implements Serializable{

	
	private static final long serialVersionUID = 1L;
	
		
	@Inject
	private EfetivaAlunoService efetivaAlunoService;
	
	@Inject
	@AlunoEdicao
	private Aluno aluno;
	
	@Inject
	private Event<AlunoAlteradoEvent> alunoAlteradoEvent;
	
	
	public void efetivaAluno()  {
		
		
			this.aluno = this.efetivaAlunoService.efetivar(this.aluno);
			this.alunoAlteradoEvent.fire(new AlunoAlteradoEvent(this.aluno));
			
			FacesUtil.addInfoMessage("Aluno efetivado com sucesso!");
		
	}

}

