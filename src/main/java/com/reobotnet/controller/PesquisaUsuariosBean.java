package com.reobotnet.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.reobotnet.model.Usuario;
import com.reobotnet.repository.Usuarios;


@Named
@ViewScoped
public class PesquisaUsuariosBean implements Serializable{

	
	private static final long serialVersionUID = 1L;
	
	@Inject
	Usuarios usuarios;
	
	private List<Usuario> usuariosList = new ArrayList<>();
	
	
	
	public Usuarios getUsuarios() {
		return usuarios;
	}


	public void setUsuarios(Usuarios usuarios) {
		this.usuarios = usuarios;
	}


	public List<Usuario> getUsuariosList() {
		return usuariosList;
	}


	public void setUsuariosList(List<Usuario> usuariosList) {
		this.usuariosList = usuariosList;
	}



	@PostConstruct
	public void inicializar() {
		
		usuariosList = usuarios.buscarTodos();
	}
	
}
