package com.reobotnet.controller;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.event.Observes;
import javax.enterprise.inject.Produces;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.lang3.StringUtils;

import com.reobotnet.model.ItemPedido;
import com.reobotnet.model.FormaPagamento;
import com.reobotnet.model.Pedido;
import com.reobotnet.model.Pessoa;
import com.reobotnet.model.Produto;
import com.reobotnet.model.Usuario;
import com.reobotnet.repository.Pessoas;
import com.reobotnet.repository.Produtos;
import com.reobotnet.repository.Usuarios;
import com.reobotnet.service.CadastroPedidoService;
import com.reobotnet.util.FacesUtil;
import com.reobotnet.validation.SKU;

@Named
@ViewScoped
public class CadastroPedidoBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private CadastroPedidoService cadastroPedidoService;

	@Inject
	private Usuarios usuarios;

	@Inject
	private Pessoas pessoas;

	@Inject
	private Produtos produtos;

	@Produces
	@PedidoEdicao
	private Pedido pedido;

	private List<Usuario> vendedores;

	private List<Pessoa> todasPessoas;

	private Produto produtoLinhaEditavel;

	private String sku;

	public CadastroPedidoBean() {
		limpar();
	}
	

	public void inicializar() {

		this.vendedores = this.usuarios.vendedores();
		this.todasPessoas = this.pessoas.todas();
		this.pedido.adicionarItemVazio();
		this.recalcularPedido();

		if (this.pedido == null) {
			this.pedido = new Pedido();
		}

	}

	private void limpar() {
		pedido = new Pedido();
		// pedido.setEnderecoEntrega(new EnderecoEntrega());
	}

	//Alterar o status do pedido e exibi a nova quantidade do produto apos a emissão
	public void pedidoAlterado(@Observes PedidoAlteradoEvent event) {
		this.pedido = event.getPedido();
	}
	
	public void salvar() {
		this.pedido.removerItemVazio();
		
		try {
			this.pedido = this.cadastroPedidoService.salvar(this.pedido);
		
			FacesUtil.addInfoMessage("Pedido salvo com sucesso!");
		} finally {
			this.pedido.adicionarItemVazio();
		}
	}
	
	//Retorna a listagem do enum formaPagamentos
 	public FormaPagamento[] getFormasPagamento() {
		return FormaPagamento.values();
	}
 	
 	//Serve para saber se é um novo pedido ou está editando
 	public boolean isEditando() {
		return this.pedido.getId() != null;
	}


 	//Recalcula o pedido 
	public void recalcularPedido() {
		if (this.pedido != null) {
			this.pedido.recalcularValorTotal();
		}
	}

	//Carrega um lista de produtos por nome
	public List<Produto> completarProduto(String nome) {
		return this.produtos.porNome(nome);
	}

	//Cria uma linha editavél p novos lançamnetos de itens
	public void carregarProdutoLinhaEditavel() {
		ItemPedido item = this.pedido.getItens().get(0);
		if (this.produtoLinhaEditavel != null) {
			if (this.existeItemComProduto(this.produtoLinhaEditavel)) {
				FacesUtil.addErrorMessage("Já existe um item no pedido com o produto informado.");
			} else {
				item.setProduto(this.produtoLinhaEditavel);
				item.setValorUnitario(this.produtoLinhaEditavel.getValorUnitario());

				this.pedido.adicionarItemVazio();
				// o null serve para limpar a linha após a inserção.
				this.produtoLinhaEditavel = null;
				this.sku = null;

				this.pedido.recalcularValorTotal();
			}
		}
	}
      
	// verifica se já existe um item no pedido com o produto informado
	private boolean existeItemComProduto(Produto produto) {
		boolean existeItem = false;

		for (ItemPedido item : this.getPedido().getItens()) {
			if (produto.equals(item.getProduto())) {
				existeItem = true;
				break;
			}
		}

		return existeItem;
	}

	//Carrega os produtos por Sku
	public void carregarProdutoPorSku() {
		if (StringUtils.isNotEmpty(this.sku)) {
			this.produtoLinhaEditavel = this.produtos.porSku(this.sku);
			this.carregarProdutoLinhaEditavel();
		}
	}
	
	// Atualiza ou remove item da lista de pedido
	public void atualizarQuantidade(ItemPedido item, int linha) {
		if (item.getQuantidade() < 1) {
			if (linha == 0) {
				item.setQuantidade(1);
			} else {
				this.getPedido().getItens().remove(linha);
			}
		}
		
		this.pedido.recalcularValorTotal();
	}
   

	public Pedido getPedido() {
		return pedido;
	}

	public void setPedido(Pedido pedido) {
		this.pedido = pedido;
	}

	public List<Usuario> getVendedores() {
		return vendedores;
	}

	public List<Pessoa> getTodasPessoas() {
		return this.todasPessoas;
	}

	public Produto getProdutoLinhaEditavel() {
		return produtoLinhaEditavel;
	}

	public void setProdutoLinhaEditavel(Produto produtoLinhaEditavel) {
		this.produtoLinhaEditavel = produtoLinhaEditavel;
	}

	@SKU
	public String getSku() {
		return sku;
	}

	public void setSku(String sku) {
		this.sku = sku;
	}

}