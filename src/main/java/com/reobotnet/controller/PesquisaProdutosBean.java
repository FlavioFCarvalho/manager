package com.reobotnet.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.reobotnet.model.Produto;
import com.reobotnet.repository.Produtos;


@Named
@ViewScoped
public class PesquisaProdutosBean implements Serializable{

	
	private static final long serialVersionUID = 1L;
	
	@Inject
	Produtos produtos;
	
	private List<Produto> produtosList = new ArrayList<>();
	
	
	
	public List<Produto> getProdutosList() {
		return produtosList;
	}
	

		
	public Produtos getProdutos() {
		return produtos;
	}



	public void setProdutos(Produtos produtos) {
		this.produtos = produtos;
	}



	public void setProdutosList(List<Produto> produtosList) {
		this.produtosList = produtosList;
	}



	@PostConstruct
	public void inicializar() {
		 produtosList = produtos.buscarTodos();
	}
	
}
