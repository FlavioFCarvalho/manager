package com.reobotnet.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.reobotnet.model.Modelo;
import com.reobotnet.repository.Modelos;



@Named
@ViewScoped
public class PesquisaModelosBean implements Serializable{

	
	private static final long serialVersionUID = 1L;
	
	@Inject
	Modelos modelos;
	
	private List<Modelo> modelosList = new ArrayList<>();
	
	
	public List<Modelo> getModelosList() {
		return modelosList;
	}



	public void setModelosList(List<Modelo> modelosList) {
		this.modelosList = modelosList;
	}


	public Modelos getModelos() {
		return modelos;
	}

	public void setModelos(Modelos modelos) {
		this.modelos = modelos;
	}


	@PostConstruct
	public void inicializar() {
		 modelosList = modelos.buscarTodos();
	}
	
}
