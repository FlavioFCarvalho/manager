package com.reobotnet.controller;


import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;



@ManagedBean(name="redBean")
@SessionScoped
public class Bean {
	private Integer opcao;
	
	private Boolean isCpf = false;
	
	public Boolean getIsCpf() {
		return isCpf;
	}
	public Integer getOpcao() {
		return opcao;
	}
	public void setOpcao(Integer opcao) {
		this.opcao = opcao;
	}
	public void renderizar() {
		if(getOpcao().equals(1)){
			isCpf = true;
		} else {
			isCpf = false;
		}
	}
	
	
}
