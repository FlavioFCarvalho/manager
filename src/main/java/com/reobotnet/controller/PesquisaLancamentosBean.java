package com.reobotnet.controller;


import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.reobotnet.model.Lancamento;
import com.reobotnet.repository.Lancamentos;
import com.reobotnet.repository.filter.LancamentoFilter;



@Named
@ViewScoped
public class PesquisaLancamentosBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	private Lancamentos lancamentos;
	
	private LancamentoFilter filtro;
	
	
	
	private List<Lancamento> lancamentosFiltrados;
	
	
	public PesquisaLancamentosBean() {
		filtro = new LancamentoFilter();
		lancamentosFiltrados = new ArrayList<>();
	}

	public void pesquisar() {
		lancamentosFiltrados = lancamentos.filtrados(filtro);         
	}
	
	
	
	public List<Lancamento> getLancamentosFiltrados() {
		return lancamentosFiltrados;
	}
	
	

	public LancamentoFilter getFiltro() {
		return filtro;
	}

	
	
 
	public String valorTotalPago() {
        int totalPago = 0;
 
        for(Lancamento lancamento : getLancamentosFiltrados()) {
            totalPago += lancamento.getValorPago(); 
            		
        }
 
        return new DecimalFormat("###,###.###").format(totalPago);
    }
 	 
	
	public String valorTotalRecebido() {
        int totalRecebido = 0;
 
        for(Lancamento lancamento : getLancamentosFiltrados()) {
            totalRecebido += lancamento.getValorRecebido(); 
            		
        }
 
        return new DecimalFormat("###,###.###").format(totalRecebido);
    }
	
	
	public String Saldo(){
		
		int saldo = 0;
		
		  return new DecimalFormat("###,###.###").format(saldo);
		
	}
	
	
}