package com.reobotnet.controller;

import java.io.Serializable;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.reobotnet.model.Banco;
import com.reobotnet.model.Custo;
import com.reobotnet.model.FormaPagamento;
import com.reobotnet.model.Lancamento;
import com.reobotnet.model.Pessoa;
import com.reobotnet.model.StatusPagamento;
import com.reobotnet.model.TipoLancamento;
import com.reobotnet.repository.Bancos;
import com.reobotnet.repository.Custos;
import com.reobotnet.repository.LancamentosOld;
import com.reobotnet.repository.Pessoas;
import com.reobotnet.service.CadastroLancamentosService;
import com.reobotnet.service.NegocioException;



@Named
@ViewScoped
public class CadastroLancamentoBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	private CadastroLancamentosService cadastroLancamentosService;
	
	@Inject
	private Pessoas pessoas;
	
	@Inject
	private Custos custos;
	
	@Inject
	private Bancos bancos;
	
	@Inject
	private LancamentosOld lancamentos;
	
	private Lancamento lancamento;
	
	private List<Pessoa> todasPessoas;
	
	private List<Custo>  todosCustos;
	
	private List<Banco> todosBancos;

	public void prepararCadastro() {
		this.todasPessoas = this.pessoas.todas();
		this.todosCustos =  this.custos.buscarTodos();
		this.todosBancos =  this.bancos.buscarTodos();
		
		if (this.lancamento == null) {
			this.lancamento = new Lancamento();
		}
	}
	
	public List<String> pesquisarDescricoes(String descricao) {
		return this.lancamentos.descricoesQueContem(descricao);
	}
	
	public void dataVencimentoAlterada(AjaxBehaviorEvent event) {
		if (this.lancamento.getDataPagamento() == null) {
			this.lancamento.setDataPagamento(this.lancamento.getDataVencimento());
		}
	}
	
	public void salvar() {
		FacesContext context = FacesContext.getCurrentInstance();
		
		try {
			this.cadastroLancamentosService.salvar(this.lancamento);
			
			this.lancamento = new Lancamento();
			context.addMessage(null, new FacesMessage("Lançamento salvo com sucesso!"));
		} catch (NegocioException e) {
			
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}
	}
	
	
		
	public List<Pessoa> getTodasPessoas() {
		return this.todasPessoas;
	}
	
		
	public List<Custo> getTodosCustos() {
		return todosCustos;
	}
	
		
	public List<Banco> getTodosBancos() {
		return todosBancos;
	}

	public TipoLancamento[] getTiposLancamentos() {
		return TipoLancamento.values();
	}
	
	public StatusPagamento[] getStatusPagamento() {
		return StatusPagamento.values();
	}
	
	public FormaPagamento[] getFormaPagamento() {
		return FormaPagamento.values();
	}
	
	public Lancamento getLancamento() {
		return lancamento;
	}

	public void setLancamento(Lancamento lancamento) {
		this.lancamento = lancamento;
	}
	
}
