package com.reobotnet.controller;

import java.io.Serializable;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.reobotnet.model.Modelo;
import com.reobotnet.service.CadastroModeloService;
import com.reobotnet.service.NegocioException;
import com.reobotnet.util.FacesUtil;


@Named
@ViewScoped
public class CadastroModeloBean implements Serializable {


	private static final long serialVersionUID = 1L;
	
	
	@Inject
	private CadastroModeloService cadastroModeloService;
	
	private Modelo modelo;
	
	
	
	public CadastroModeloBean() {
		limpar();
	}
	
	private void limpar() {
		modelo = new Modelo();
	}
	
		
	public void salvar() throws NegocioException{
		this.modelo = cadastroModeloService.salvar(this.modelo);
		limpar();
		FacesUtil.addInfoMessage("Modelo salvo com sucesso!");
	}

	public Modelo getModelo() {
		return modelo;
	}

	public void setModelo(Modelo modelo) {
		this.modelo = modelo;
	}

	

}
