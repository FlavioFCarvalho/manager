package com.reobotnet.controller;

import java.io.Serializable;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.reobotnet.model.Orcamento;
import com.reobotnet.model.OrcamentoItem;
import com.reobotnet.service.CadastroOrcamentoService;
import com.reobotnet.util.FacesUtil;

@Named
@ViewScoped
public class CadastroOrcamentoBean implements Serializable{

	
	private static final long serialVersionUID = 1L;
	
	@Inject
	private CadastroOrcamentoService cadastroOrcamentoService;
	
	private Orcamento orcamento = new Orcamento();
	
	private OrcamentoItem item;
	
	public void novoItem(){
		
		item = new OrcamentoItem();
	}
	
	public void adicionarItem(){
		
		orcamento.getItens().add(item);
		item.setOrcamento(orcamento);
	}

	public Orcamento getOrcamento() {
		return orcamento;
	}
	
	
	
	public OrcamentoItem getItem() {
		return item;
	}

	public void salvar(){
		
		cadastroOrcamentoService.salvar(orcamento);
		orcamento = new Orcamento();
		FacesUtil.addInfoMessage("Orçamento salvo com sucesso!");
		
	}

}
