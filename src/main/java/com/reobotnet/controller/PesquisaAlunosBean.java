package com.reobotnet.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.reobotnet.model.Aluno;
import com.reobotnet.model.StatusAluno;
import com.reobotnet.repository.Alunos;
import com.reobotnet.repository.filter.AlunoFilter;



@Named
@ViewScoped
public class PesquisaAlunosBean implements Serializable{

	
	private static final long serialVersionUID = 1L;
	
	@Inject
	private Alunos alunos;
	
	private AlunoFilter filtro;
	private List<Aluno> alunosFiltrados;
	
	public PesquisaAlunosBean() {
		
		filtro = new AlunoFilter();
		alunosFiltrados = new ArrayList<>();
	}

	public void pesquisar() {
		alunosFiltrados = alunos.filtrados(filtro);
	}
	
	public StatusAluno[] getStatuses() {
		return StatusAluno.values();
	}
	
	public List<Aluno> getAlunosFiltrados() {
		return alunosFiltrados;
	}

	public AlunoFilter getFiltro() {
		return filtro;
	}
}
