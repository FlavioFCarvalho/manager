package com.reobotnet.controller;

import java.io.Serializable;

import java.text.DecimalFormat;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.reobotnet.model.Lancamento;
import com.reobotnet.repository.LancamentosOld;


@Named
@ViewScoped
public class PesquisaLancamentosBeanOld implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private LancamentosOld lancamentosRepository;
	
		
	private List<Lancamento> lancamentos;
	
	private Lancamento lancamentoSelecionado;
	
	public void consultar() {
		this.lancamentos = lancamentosRepository.todos(); 
	}
	
	public List<Lancamento> getLancamentos() {
		return lancamentos;
	}

	public Lancamento getLancamentoSelecionado() {
		return lancamentoSelecionado;
	}

	public void setLancamentoSelecionado(Lancamento lancamentoSelecionado) {
		this.lancamentoSelecionado = lancamentoSelecionado;
	}
	
	public String valorTotalPago() {
        int total = 0;
 
        for(Lancamento lancamento : getLancamentos()) {
            total += lancamento.getValorPago(); 
            		
        }
 
        return new DecimalFormat("###,###.###").format(total);
    }
 	 
	
	public String valorTotalRecebido() {
        int total = 0;
 
        for(Lancamento lancamento : getLancamentos()) {
            total += lancamento.getValorRecebido(); 
            		
        }
 
        return new DecimalFormat("###,###.###").format(total);
    }

}