package com.reobotnet.controller;

import java.io.Serializable;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.reobotnet.model.Servico;
import com.reobotnet.model.Status;

import com.reobotnet.service.NegocioException;
import com.reobotnet.service.CadastroServicoService;
import com.reobotnet.util.FacesUtil;

@Named
@ViewScoped
public class CadastroServicoBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private CadastroServicoService cadastroServicoService;

	private Servico servico;

	public CadastroServicoBean() {
		limpar();
	}

	private void limpar() {
		servico = new Servico();
	}

	public void salvar() throws NegocioException {
		this.servico = cadastroServicoService.salvar(this.servico);
		limpar();
		FacesUtil.addInfoMessage("serviço salvo com sucesso!");
	}

	public Servico getServico() {
		return servico;
	}

	public void setServico(Servico servico) {
		this.servico = servico;
	}
	
	public Status[] getStatus() {
		return Status.values();
	}


}
