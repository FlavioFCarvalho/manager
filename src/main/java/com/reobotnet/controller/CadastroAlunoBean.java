package com.reobotnet.controller;

import java.io.Serializable;

import javax.enterprise.event.Observes;
import javax.enterprise.inject.Produces;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.reobotnet.model.Aluno;
import com.reobotnet.model.Sexo;
import com.reobotnet.model.Status;
import com.reobotnet.service.CadastroAlunoService;
import com.reobotnet.service.NegocioException;
import com.reobotnet.util.FacesUtil;




@Named
@ViewScoped
public class CadastroAlunoBean implements Serializable {


	private static final long serialVersionUID = 1L;
	
	
	@Inject
	private CadastroAlunoService cadastroAlunoService;
	
	@Produces
	@AlunoEdicao
	private Aluno aluno;
	
	
	
	public CadastroAlunoBean() {
		limpar();
	}
	
	private void limpar() {
		aluno = new Aluno();
	}
	
	//Alterar o status do aluno apos a emissão
		public void alunoAlterado(@Observes AlunoAlteradoEvent event) {
			this.aluno = event.getAluno();
		}
		
	public void salvar() throws NegocioException{
		this.aluno = cadastroAlunoService.salvar(this.aluno);
		limpar();
		FacesUtil.addInfoMessage("Aluno salvo com sucesso!");
	}

	public Aluno getAluno() {
		return aluno;
	}

	public void setAluno(Aluno aluno) {
		this.aluno = aluno;
	}
	
	public Sexo[] getSexo() {
		return Sexo.values();
	}

}
